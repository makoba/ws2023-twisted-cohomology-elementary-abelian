# ws2023-twisted-cohomology-elementary-abelian

These are notes for a talk I am giving in the seminar on tt-geometry in Bielefeld on the geometry of permutation modules (organized by Juan Omar Gomez).
A compiled version can be found [here](https://makoba.gitlab.io/ws2023-twisted-cohomology-elementary-abelian/elementary-abelian.pdf).