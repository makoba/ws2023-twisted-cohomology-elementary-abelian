\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{Twisted cohomology for elementary Abelian $p$-groups}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I am giving in the seminar on tt geometry on \emph{the geometry of permutation modules} taking place in the winter term 2023/2024 in Bielefeld.
    The main reference for this talk is \cite[Section 13-15]{balmer-gallauer}.

    I thank the organizer of the seminar, Juan Omar Gomez.
    Also, use these notes at your own risk, they probably contain mistakes (feel free to tell me about these mistakes)!
    
    \section{Setup and Recollections}

    We fix a field $k$ of positive characteristic $p > 2$ and a finite group $G$ (at some points we will specialize to the case when $G = E$ is an elementary Abelian $p$-group).

    \begin{itemize}
        \item
        $\calD(G)$ denotes the \emph{bounded derived category of finite dimensional $kG$-modules}.
        The ($\ZZ$-graded graded-commutative) endomorphism ring of its tensor unit is given by the \emph{cohomology ring} $H^{\bullet}(G, k)$ and the natural comparison map
        \[
            \Spc \roundbr[\big]{\calD(G)} \to \Spec^{\h} \roundbr[\big]{H^{\bullet}(G, k)}
        \]
        is a homeomorphism, as was proven by Benson, Carson and Rickard.

        The \emph{Balmer spectrum} $\Spc(\calD(G))$ is a local topological space; its closed point $0 \subseteq \calD(G)$ corresponds to the augmentation ideal $H^+(G, k) \subseteq H^{\bullet}(G, k)$.
        It is also the kernel of the forgetful functor
        \[
            \calD(G) \to \calD(1).
        \]

        \item
        $\calK(G)$ denotes the tt category that is the \emph{idempotent completion of the homotopy category of bounded complexes of finite dimensional permutation $kG$-modules}.

        \item
        We have a natural \emph{localization functor}
        \[
            \calK(G) \to \calD(G)
        \]
        from $\calK(G)$ to the bounded derived category of all finite dimensional $kG$-modules $\calD(G)$.
        It induces an open immersion
        \[
            V_G \coloneqq \Spc \roundbr[\big]{\calD(G)} \to \Spc \roundbr[\big]{\calK(G)}.
        \]
        We call $V_G \subseteq \Spc(\calK(G))$ the \emph{cohomological open}.

        \item
        Given a $p$-subgroup $H \subseteq G$ we constructed the so called \emph{modular fixed points functor}
        \[
            \Psi^{H; G} \colon \calK(G) \to \calK(G \sslash H).
        \]
        It induces a closed continuous map
        \[
            \psi^{H; G} \colon \Spc \roundbr[\big]{\calK(G \sslash H)} \to \Spc \roundbr[\big]{\calK(G)}
        \]
        that it is a closed immersion when $H \subseteq G$ is normal.

        We write $\calM(H) \in \Spc(\calK(G))$ for the image under $\psi^{H; G}$ of the unique closed point in $V_{G \sslash H}$, or in other words the kernel of the composition
        \[
            \FF^H \colon \calK(G) \xrightarrow{\Psi^{H; G}} \calK(G \sslash H) \to \calD(1).
        \]
        In fact the point $\calM(H)$ is closed in $\Spc(\calK(G))$.

        \item
        In any case, the restriction of $\psi^{H; G}$ to the cohomological open $V_{G \sslash H}$ is injective and in fact we have the description
        \[
            \Spc \roundbr[\big]{\calK(G)} = \bigsqcup_{H \in \Sub_p(G)/G} V_{G \sslash H}
        \]
        as sets.
        In other words, every point in $\Spc(\calK(G))$ is of the form $\calP(H, \frakp)$ for some $p$-subgroup $H$ and a homogeneous prime ideal $\frakp \subseteq H^{\bullet}(G \sslash H, k)$, in a unique way up to conjugation.

        The closed points in $\Spc(\calK(G))$ are precisely the points $\calM(H)$ for $H \in \Sub_p(G)/G$.
    \end{itemize}

    \section{Generalities on tt categories} \label{sec:tt-cats}

    Let $\calT$ be a rigid tt category.
    Write
    \[
        R^{\bullet} \coloneqq \bigoplus_{s \in \ZZ} \Hom_{\calT}\roundbr[\big]{\bone, \bone[s]}
    \]
    for the ($\ZZ$-graded graded-commutative) endomorphism ring of the tensor unit $\bone \in \calT$.

    \begin{theorem} \label{thm:comp}
        There is a natural continuous \emph{comparison map}
        \[
            \comp_{\calT} \colon \Spc(\calT) \to \Spec^{\h} \roundbr[\big]{R^{\bullet}}, \qquad \calP \mapsto \bigoplus_{s \in \ZZ} \set[\big]{a \colon \bone \to \bone[s]}{\cone(a) \notin \calP}.
        \]
    \end{theorem}

    \begin{proof}
        See \cite[Theorem 5.3]{balmer}.
    \end{proof}

    \begin{theorem} \label{thm:comp-closed-surjective}
        Suppose that $\calT$ is End-finite.
        The latter means that $R^{\bullet}$ is Noetherian and that for every $X, Y \in \calT$ the (graded) $R^{\bullet}$-module $\Hom^{\bullet}_{\calT}(X, Y)$ is finitely generated.

        Then $\comp_{\calT}$ is a closed surjection.
    \end{theorem}

    \begin{proof}
        See \cite[Theorem 7.3]{balmer} and \cite[Proposition 2.7]{lau}.
    \end{proof}

    Now let us fix a finite set $I$ and a tuple $u = (u_i)_{i \in I}$ of invertible objects in $\calT$.
    We assume that the switch automorphism of $u_i \otimes u_i$ is given by the identity for all $i \in I$.

    Write
    \[
        R_u^{\bullet, \bullet} \coloneqq \bigoplus_{s \in \ZZ} \bigoplus_{q \in \NN^I} \Hom_{\calT}\roundbr[\big]{\bone, \bone(q)[s]},
    \]
    where we have set
    \[
        \bone(q) \coloneqq \bigotimes_{i \in I} u_i^{\otimes q(i)}.
    \]
    This is a $\ZZ \times \NN^I$-graded ring that is graded commutative in the shift and commutative in the twist.

    \begin{proposition} \label{prop:twist-comp}
        There is a natural continuous \emph{comparison map}
        \[
            \comp_{\calT, u} \colon \Spc(\calT) \to \Spec^{\h}(R_u^{\bullet, \bullet}), \qquad \calP \mapsto \bigoplus_{s \in \ZZ} \bigoplus_{q \in \NN^I} \set[\big]{a \colon \bone \to \bone(q)[s]}{\cone(a) \notin \calP}.
        \]
    \end{proposition}

    \begin{proof}
        This is similar to \Cref{thm:comp}, see also \cite[Proposition 13.4]{balmer-gallauer}.
    \end{proof}

    \begin{lemma} \label{lem:twist-comp}
        For each $i \in I$, let $n_i \in 2 \ZZ$ and $t_i \colon \bone \to \bone(e_i)[n_i]$.
        Set $\calT' \coloneqq \calT \squareset{t_i^{-1}}{i \in I}^{\natural}$.
        Then we have a natural isomorphism
        \[
            R_{\calT'}^{\bullet} \cong \roundbr[\big]{R_{\calT, u}^{\bullet, \bullet}\squareset{t_i^{-1}}{i \in I}}_{\zerotwist}
        \]
        and a cartesian square
        \[
        \begin{tikzcd}
            \Spc(\calT') \ar[r] \ar[d, "\comp_{\calT'}"]
            & \Spc(\calT) \ar[d, "\comp_{\calT, u}"]
            \\
            \Spec^{\h}\roundbr[\big]{R_{\calT'}^{\bullet}} \ar[r]
            & \Spec^{\h} \roundbr[\big]{R_{\calT, u}^{\bullet, \bullet}}
        \end{tikzcd}.
        \]
        Here the lower map is induced by the ring homomorphism
        \[
            R_{\calT, u}^{\bullet, \bullet} \to R_{\calT'}^{\bullet}, \qquad \roundbr[\big]{a \in R_{\calT, u}^{s, q}} \mapsto a \cdot \prod_{i \in I} t_i^{- q(i)}
        \]
        that is graded for the morphism of commutative monoids
        \[
            \ZZ \times \NN^I \to \ZZ, \qquad (s, q) \mapsto s - \sum_{i \in I} n_i q(i)
        \]
        and the horizontal maps are open immersions.
    \end{lemma}

    \begin{proof}
        Compare with \cite[Proposition 13.4]{balmer-gallauer}.
    \end{proof}

    \section{Twisted cohomology}

    Write $\calN(G)$ for the set of normal subgroups $N \subseteq G$ of index $p$.
    For each $N \in \calN(G)$ we choose a generator $\sigma_N$ of the cyclic group $G/N$ and write $\tau_N \coloneqq \sigma - 1 \in k(G/N)$.

    \begin{definition}
        Let $N \in \calN(G)$.
        Then we define the object
        \[
            u_N \coloneqq \roundbr[\big]{k(G/N) \xrightarrow{\tau_N} k(G/N) \xrightarrow{1} k(G/G)} \in \calK(G).
        \]
        Here the term $k = k(G/G)$ is sitting in (homological) degree $0$.

        We define morphisms
        \[
            a_N \colon \bone \to u_N, \quad b_N \colon \bone \to u_N[-2] \quad \text{and} \quad c_N \colon \bone \to u_N[-1]
        \]
        that are given by
        \[
        \begin{tikzcd}
            0 \ar[r] \ar[d]
            & 0 \ar[r] \ar[d]
            & k \ar[d, "1"]
            \\
            k(G/N) \ar[r, "\tau_N"]
            & k(G/N) \ar[r, "1"]
            & k(G/G)
        \end{tikzcd},
        \]
        \[
        \begin{tikzcd}
            k \ar[r] \ar[d, "\tau_N^{p - 1}"]
            & 0 \ar[r] \ar[d]
            & 0 \ar[d]
            \\
            k(G/N) \ar[r, "\tau_N"]
            & k(G/N) \ar[r, "1"]
            & k(G/G)
        \end{tikzcd}
        \]
        and
        \[
        \begin{tikzcd}
            0 \ar[r] \ar[d]
            & k \ar[r] \ar[d, "\tau_N^{p - 1}"]
            & 0 \ar[d]
            \\
            k(G/N) \ar[r, "\tau_N"]
            & k(G/N) \ar[r, "1"]
            & k(G/G)
        \end{tikzcd}
        \]
        respectively.

        We also set $u \coloneqq (u_N)_{N \in \calN(G)}$.
    \end{definition}

    \begin{lemma}
        Let $N \in \calN(G)$.
        The object $u_N \in \calK(G)$ is invertible and the switch automorphism of $u_N \otimes u_N$ is the identity.
    \end{lemma}

    \begin{proof}
        This can be done by an explicit computation; one may use the conservativity of the modular fixed point functor to simplify this computation.
        Compare with \cite[Section 12]{balmer-gallauer}.
    \end{proof}

    \begin{definition}
        We define the \emph{twisted cohomology ring} of $G$ as
        \[
            H^{\bullet, \bullet}(G, k) \coloneqq R_{\calK(G), u}^{\bullet, \bullet}.
        \]
    \end{definition}

    \begin{theorem} \label{thm:twisted-coh-fin-gen}
        The $k$-algebra $H^{\bullet, \bullet}(G, k)$ is generated by the (finitely many) elements $a_N$, $b_N$, $c_N$ for $N \in \calN(G)$.
        In particular $H^{\bullet, \bullet}(G, k)$ is Noetherian.
    \end{theorem}

    \begin{proof}
        This is proven by a computation, using induction. See \cite[Lemma 12.12]{balmer-gallauer}.
    \end{proof}

    \begin{example}
        For $G = C_p$ we have
        \[
            H^{\bullet, \bullet}(G, k) \cong k[a, b, c]/(c^2).
        \]
    \end{example}

    \begin{definition}
        Let $H \in \Sub_p(G)/G$.
        Set
        \[
            S_H \coloneqq \set[\big]{a_N}{H \not\subseteq N} \cup \set[\big]{b_N}{H \subseteq N}.
        \]
        Then define
        \[
            \calL_G(H) \coloneqq \calK(G)\squarebr[\big]{S_H^{-1}}^{\natural}
        \]
        and
        \[
            U_G(H) \coloneqq \Spc \roundbr[\big]{\calL_G(H)} = \bigcap_{H \not\subseteq N} \open(a_N) \cap \bigcap_{H \subseteq N} \open(b_N) \subseteq \Spc \roundbr[\big]{\calK(G)}.
        \]
        Also define
        \[
            \calO_G^{\bullet}(H) \coloneqq R_{\calL_G(H)}^{\bullet} = \roundbr[\big]{H^{\bullet, \bullet}(G, k)[S_H^{-1}]}_{\zerotwist}.
        \]
        Compare this with the material from \Cref{sec:tt-cats}.
    \end{definition}

    \begin{proposition}
        Let $H \in \Sub_p(G)/G$.
        Then we have $\calM(H) \in U_G(H)$.

        Consequently the family $(U_G(H))_{H \in \Sub_p(G)/H}$ is an open cover of $\Spc(\calK(G))$.
    \end{proposition}

    \begin{proof}
        Recall that $\calM(H) \in \Spc(\calK(G))$ is the kernel of the functor
        \[
            \FF^H \colon \calK(G) \xrightarrow{\Psi^{H; G}} \calK(G \sslash H) \to \calD(1).
        \]
        We thus need to show that the $a_N$ for $H \not\subseteq N$ and the $b_N$ for $H \subseteq N$ become quasi-isomorphisms after applying $\Psi^{H; G}$.

        So let $N \in \calN(G)$.
        Then we have
        \[
            (G/N)^H =
            \begin{cases}
                \emptyset & \text{if $H \not\subseteq N$,}
                \\
                G/N & \text{if $H \subseteq N$;}
            \end{cases}
        \]
        consequently we obtain
        \[
            \Psi^{H; G}(u_N) =
            \begin{cases}
                k(G/G) & \text{if $H \not\subseteq N$,}
                \\
                k(G/N) \to k(G/N) \to k(G/G) & \text{if $H \subseteq N$}.
            \end{cases}
        \]
        It is then immediate that $a_N$ and $b_N$ become quasi-isomorphisms in the respective cases.
    \end{proof}

    From now on let us assume that $G = E$ is an elementary Abelian $p$-group.

    \begin{proposition} \label{prop:1-coh-open}
        $U_E(1) = V_E$ is the cohomological open in $\Spc(\calK(E))$.
        Consequently we have $\calL_E(1) \cong \calD(E)$.
    \end{proposition}

    \begin{proof}
        Let $N \in \calN(G)$.
        By \cite[Lemma 13.2]{balmer-gallauer}, the two objects $\kos_E(N)$ and $\cone(b_N)$ generate the same tt ideal in $\calK(E)$ (this tt ideal is also the kernel of $\calK(E) \to \calK(N)$).

        Now let $H \subseteq E$ be a subgroup, let $\frakp \subseteq H^{\bullet}(E/H, k)$ be a homogeneous prime ideal and consider the associated point $\calP(H, \frakp) \in \Spc(\calK(G))$.
        Then $\calP(H, \frakp)$ is contained in $U_E(1)$ if and only if we have $\cone(b_N) \in \calP(H, \frakp)$ for all $N \in \calN(E)$.
        This condition now is equivalent to the condition $\kos_E(N) \in \calP(H, \frakp)$ which is in turn equivalent to the condition $H \subseteq N$ by \cite[Lemma 7.12]{balmer-gallauer}.

        Now the only subgroup of $E$ contained in all $N \in \calN(E)$ is the trivial subgroup.
        This finishes the proof.
    \end{proof}

    \begin{corollary} \label{cor:injective-1}
        Let $H \subseteq E$ be a subgroup.
        Then the composition
        \[
            V_{E/H} \subseteq \Spc \roundbr[\big]{\calK(E)} \to \Spec^{\h} \roundbr[\big]{H^{\bullet, \bullet}(E, k)}
        \]
        is injective.
    \end{corollary}

    \begin{proof}
        Using that the surjective homomorphism of graded rings $H^{\bullet, \bullet}(E, k) \to H^{\bullet, \bullet}(E/H, k)$ induces a closed immersion
        \[
            \Spec^{\h} \roundbr[\big]{H^{\bullet, \bullet}(E/H, k)} \to \Spec^{\h} \roundbr[\big]{H^{\bullet, \bullet}(E, k)}
        \]
        we can reduce to the case $H = 1$.
        But then the above map factors as
        \[
            V_E \cong \Spec^{\h} \roundbr[\big]{H^{\bullet}(E, k)} \to \Spec^{\h} \roundbr[\big]{H^{\bullet, \bullet}(E, k)}
        \]
        where the second map is an open immersion by \Cref{lem:twist-comp}, applying \Cref{prop:1-coh-open}.
    \end{proof}

    \begin{corollary} \label{cor:injective-2}
        The map
        \[
            \Spc \roundbr[\big]{\calK(E)} \to \Spec^{\h} \roundbr[\big]{H^{\bullet, \bullet}(E, k)}
        \]
        is injective.
    \end{corollary}

    \begin{proof}
        Let $H \subseteq E$ be a subgroup, let $\frakp \subseteq H^{\bullet}(E/H, k)$ be a homogeneous prime ideal and let $N \in \calN(E)$.
        From the proof of \Cref{prop:1-coh-open} it follows that
        \[
            \calP(H, \frakp) \in \open(b_N) \Longleftrightarrow H \subseteq N.
        \]
        Thus, if two points $\calP(H, \frakp), \calP(H', \frakp') \in \Spc(\calK(E))$ have the same image in $\Spec^{\h}(H^{\bullet, \bullet}(E, k))$ then the subgroups $H, H' \subseteq E$ have to be contained in exactly the same $N \in \calN(E)$.
        This forces $H = H'$.
        But then we also have $\frakp = \frakp'$ by \Cref{cor:injective-1}.
    \end{proof}

    \begin{theorem}
        Let $H \subseteq E$ be a subgroup.
        Then the map
        \[
            U(H) \to \Spec^{\h} \roundbr[\big]{\calO_E^{\bullet}(H)}
        \]
        is a homeomorphism.
        Consequently the map $\Spc(\calK(E)) \to \Spec^{\h}(H^{\bullet, \bullet}(E, k))$ is an open immersion with image
        \[
            \set[\Big]{\frakp \in \Spec^{\h}\roundbr[\big]{H^{\bullet, \bullet}(E, k)}}{\text{we have $a_N \notin \frakp$ or $b_N \notin \frakp$ for all $N \in \calN(E)$}}.
        \]
    \end{theorem}

    \begin{proof}
        We already know that $U(H) \to \Spec^{\h}(\calO_E^{\bullet}(H))$ is injective by \Cref{cor:injective-2}.
        By \Cref{thm:comp-closed-surjective} it now suffices to show that $\calL_E(H)$ is End-finite.

        By \Cref{thm:twisted-coh-fin-gen} we already know that $\calO_E^{\bullet}(H)$ is Noetherian.
        To see that $\Hom_{\calL_E(H)}^{\bullet}(X, Y)$ is finitely generated for all $X, Y \in \calL_E(H)$ we now show that $\calL_E(H)$ is generated by the tensor unit $\bone$.
        
        So let $\calJ \subseteq \calL_E(H)$ be the thick triangulated subcategory generated by $\bone$ and note that $\calJ \subseteq \calL_E(H)$ is in fact a tt subcategory.
        Clearly it suffices to show that $k(E/H) \in \calJ$ for all subgroups $H \in E$.
        But this object is isomorphic to a tensor product of objects $k(E/N)$ for some $N \in \calN(E)$.
        To see that $k(E/N) \in \calJ$ we now consider the following two cases.

        \begin{itemize}
            \item
            Suppose that $H \not\subseteq N$.
            Then $a_N$ is invertible in $\calL_E(H)$ so that we have
            \[
                0 = \cone(a_N) \cong \roundbr[\big]{k(E/N) \xrightarrow{\tau_N} k(E/N)}.
            \]
            Thus $\tau_N \colon k(E/N) \to k(E/N)$ is an isomorphism in $\calL_E(H)$.
            As it is also nilpotent we necessarily have $k(E/N) = 0 \in \calJ$.

            \item
            Suppose now that $H \subseteq N$.
            Then $b_N$ is an isomorphism in $\calL_E(H)$ so that $u_N \in \calJ$.
            It follows that $a_N$ is a morphism in $\calJ$ so that also
            \[
                \cone(a_N) \cong \roundbr[\big]{k(E/N) \xrightarrow{\tau_N} k(E/N)} \in \calJ.
            \]
            As $\tau_N$ is nilpotent, the octahedron axiom then implies that we have $k(E/N) \in \calJ$. \qedhere
        \end{itemize}
    \end{proof}

    \printbibliography
\end{document}